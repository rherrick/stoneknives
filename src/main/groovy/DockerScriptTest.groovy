import org.nrg.xnat.dsl.RunXnatDslScript

import java.nio.file.Paths

if (!args || args[0] == null || args[0] == "") {
    throw new RuntimeException("You must specify a file name.")
}

def File file = Paths.get(args[0]).toFile()
if (!file.exists()) {
    throw new RuntimeException("Couldn't find the file ${file.absolutePath}.")
}

def buffer = new StringBuilder()
file.eachLine {
    buffer.append(it).append("\n")
}

def run = new RunXnatDslScript()
run.dslClassId = "XnatDocker"
run.text = buffer.toString()
println run.run()
