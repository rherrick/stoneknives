import groovy.json.JsonSlurper

def json = '[{"uid":10512213, "name":"Bob"},{"uid":7208201, "name":"John"},{"uid":10570, "name":"Jim"},{"uid":1799657, "name":"Sally"}]'
def slurper = new JsonSlurper()
def thing = slurper.parseText(json)
thing.each { person ->
    println "UID: ${person.uid} Name: ${person.name}"
}
def bob = thing.find {
    uid = 10512213
}
println bob
