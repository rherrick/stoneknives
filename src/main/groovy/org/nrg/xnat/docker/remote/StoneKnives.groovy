package org.nrg.xnat.docker.remote
import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient
import com.spotify.docker.client.LogStream
import com.spotify.docker.client.messages.ContainerConfig
import com.spotify.docker.client.messages.ContainerCreation
import com.spotify.docker.client.messages.ContainerInfo
import com.spotify.docker.client.messages.ContainerState
import com.spotify.docker.client.messages.HostConfig


def String[] cmd = "/opt/run -z y -f %i_%s -o /output /input".split(" ")
def String[] idle = "sh -c while :; do sleep 1; echo \$(date) >> /output/heartbeat.log; done".split(" ")
def final HostConfig host = HostConfig.builder().binds("/data/xnat/cache/prj001:/input", "/data/xnat/cache/nii:/output").build();
def final ContainerConfig config = ContainerConfig.builder().image("pooky-dev/dcm2niix:latest").hostConfig(host).attachStdout(true).attachStderr(true).cmd(cmd).build();
def final DockerClient client = DefaultDockerClient.builder().uri("http://localhost:2375").build();
def final ContainerCreation container = client.createContainer(config);

final String id = container.id();

// Inspect container
final ContainerInfo info = client.inspectContainer(id);
def final ContainerState state = info.state()
println "Config: ${config}"
println "Info: ${info}"
println "Path: ${info.path()}"
println "Args: ${info.args()}"

// Start container
client.startContainer(id);

final LogStream output = client.logs(id, DockerClient.LogsParameter.STDERR, DockerClient.LogsParameter.STDOUT)
final String execOutput = output.readFully()
println execOutput

// Exec command inside running container with attached STDOUT and STDERR
// def final String[] cmd = "find /data /input /output".split(" ")
client.execCreate(id, cmd, DockerClient.ExecParameter.STDOUT, DockerClient.ExecParameter.STDERR);
//final LogStream output = client.execStart(execId);
//final String execOutput = output.readFully();
//
//println execOutput

// Kill container
client.killContainer(id);

// Remove container
client.removeContainer(id);
