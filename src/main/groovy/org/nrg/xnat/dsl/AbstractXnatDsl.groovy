package org.nrg.xnat.dsl

abstract class AbstractXnatDsl {
    abstract def String launch(Closure closure)
}
