package org.nrg.xnat.dsl
import groovy.util.logging.Slf4j
import org.apache.commons.lang3.StringUtils
import org.codehaus.groovy.control.CompilerConfiguration
import org.nrg.xnat.dsl.annotations.XnatDsl
import org.nrg.xnat.dsl.docker.XnatDockerDsl
import org.reflections.Reflections
import org.reflections.scanners.SubTypesScanner
import org.reflections.scanners.TypeAnnotationsScanner
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder

import java.nio.file.Paths

@Slf4j
class RunXnatDslScript {

    def String dslClassName
    def String dslClassId
    def String filePath
    def String text
    def Reader reader

    def String run(Map parameters) {
        def Class<? extends AbstractXnatDsl> dslClass = getDslClass()
        if (dslClass == null) {
            throw new RuntimeException("You must set the ID or class name of your DSL definition to run the script.")
        }
        def Reader script = getScript()
        if (script == null) {
            throw new RuntimeException("You must specify the DSL script to be executed.")
        }
        def config = new CompilerConfiguration()
        config.scriptBaseClass = DelegatingScript.class.name
        def binding = new Binding(parameters)
        binding.setProperty("item", null)
        def shell = new GroovyShell(binding, config)
        def DelegatingScript exec = shell.parse(script) as DelegatingScript
        exec.setDelegate(dslClass.newInstance())
        exec.run()
    }

    private Class<? extends AbstractXnatDsl> getDslClass() {
        if (StringUtils.isNotBlank(dslClassId)) {
            log.info("Retrieving DSL delegate class by ID ${dslClassId}")
            def types = reflections.getTypesAnnotatedWith(XnatDsl.class)
            types.find { type ->
                type.getAnnotation(XnatDsl.class).value() == dslClassId
            } as Class<? extends AbstractXnatDsl>
        } else if (StringUtils.isNotBlank(dslClassName)) {
            log.info("Retrieving DSL delegate class by name ${dslClassName}")
            Class.forName(dslClassName, true, Thread.currentThread().getContextClassLoader()).newInstance() as Class<? extends AbstractXnatDsl>
        } else {
            log.warn("No value set for ID or class name of DSL class, returning null")
            null
        }
    }

    private Reader getScript() {
        if (StringUtils.isNotBlank(filePath)) {
            def File file = Paths.get(filePath).toFile()
            if (!file.exists()) {
                throw new RuntimeException("Can't find the file specified by ${filePath}.")
            }
            if (!file.isDirectory()) {
                throw new RuntimeException("The path specified by ${filePath} is a folder, not a file.")
            }
            new FileReader(file)
        } else if (StringUtils.isNotBlank(text)) {
            new StringReader(text)
        } else if (reader != null) {
            reader
        } else {
            null
        }
    }

    final static Reflections reflections = new Reflections(new ConfigurationBuilder()
            .setUrls(ClasspathHelper.forPackage("org.nrg.xnat.dsl"))
            .setScanners(new SubTypesScanner(), new TypeAnnotationsScanner()));
}
