package org.nrg.xnat.dsl.docker

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient
import com.spotify.docker.client.LogStream
import com.spotify.docker.client.messages.ContainerConfig
import com.spotify.docker.client.messages.ContainerCreation
import com.spotify.docker.client.messages.HostConfig
import groovy.util.logging.Slf4j
import org.nrg.xnat.dsl.AbstractXnatDsl
import org.nrg.xnat.dsl.annotations.XnatDsl

@Slf4j
@XnatDsl("XnatDocker")
class XnatDockerDsl extends AbstractXnatDsl {
    def String[] idle = "sh -c while :; do sleep 1; done".split(" ")

    def String uri
    def String imageId
    def String imageVersion
    def String command
    def Map<String, String> mounts = [:]
    def boolean async = false

    @Override
    def String launch(Closure closure) {
        log.info("Running ${imageId}:${imageVersion:"latest"} on ${uri} with command ${command}. Volumes:\n${mounts.each { key, value -> println " * ${key}: ${value}" }}")
        def final HostConfig host = HostConfig.builder().binds(mounts.collect { source, target -> "${source}:${target}".toString() }).build()
        def final ContainerConfig config = ContainerConfig.builder().image("${imageId}:${imageVersion?:'latest'}").hostConfig(host).attachStdout(true).attachStderr(true).cmd(async ? idle : command.split(" ")).build()
        def final DockerClient client = DefaultDockerClient.builder().uri(uri).build()
        def final ContainerCreation container = client.createContainer(config)

        final String id = container.id();

        // Start container
        log.info("Starting container ${id}")
        client.startContainer(id);

        // Exec command inside running container with attached STDOUT and STDERR
        final LogStream output
        if (async) {
            def final String execId = client.execCreate(id, command.split(" "), DockerClient.ExecParameter.STDOUT, DockerClient.ExecParameter.STDERR)
            output = client.execStart(execId);
        } else {
            output = client.logs(id, DockerClient.LogsParameter.STDERR, DockerClient.LogsParameter.STDOUT)
        }

        log.info("Waiting until container ${id} stops")
        client.waitContainer(id)

        def String result = output.readFully()
        log.info("Execution gave result ${result}")

        // Kill container
        log.info("Killing container ${id}")
        client.killContainer(id)

        // Remove container
        log.info("Removing container ${id}")
        client.removeContainer(id)

        client.close()

        result
    }
}
